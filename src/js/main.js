$(function () {

	$( ".header__menu-list > .header__menu-item" ).hover (function() {
	    $(this).children().next().toggleClass('show');
	    $(this).find('.header__menu-link').toggleClass('active');

	    if ($(this).children('.header__menu-dropdown').length > 0) {
	    	$(this).toggleClass('helper');
	    }
	})

	if($('.review-slider').length > 0) {
		$('.review-slider').slick({
		    slidesToShow: 1,
		    infinite: true,
		    dots: false,
		    arrows: false,
		    speed: 500,
		    fade: true,
		    cssEase: 'linear',
		    slidesToScroll: 1,
		    responsive: [
		        {
		            breakpoint: 758,
		            settings: {
		                dots: true,
		            }
		        },
		    ]
		});

		$(".review__slider-btn--prev").on("click", function() {
		    $(".review-slider").slick("slickPrev");
		});

		$(".review__slider-btn--next").on("click", function() {
		    $(".review-slider").slick("slickNext");
		});
	}




	$('.burger-content').on('click', function () {
	  $('.header__menu').toggleClass('active');
    $('.header__mobile-call').toggleClass('hide');
    $('.header__mobile-cabinet').toggleClass('hide');
    $('.burger-mob').toggleClass('active');

	  // $('body').toggleClass('overflowHidden');
	});
  


  	if ($(window).width() < 1020) {
  		$('.header__menu > ul > li > a').on('click', function (e) {
  		    var $this = $(this);
  		    if ($this.next('.header__menu-dropdown').length > 0 ) {
  		    	e.preventDefault();
  		    	$this.toggleClass('is-open');
  		    	$this.next('.header__menu-dropdown').slideToggle();
  		    }
  		})
  	}


  	if ($(window).width() < 580) {
  		$('.footer__menu-title').on('click', function (e) {
  		    var $this = $(this);
  		    if ($this.next('.footer__menu-lists').length > 0 ) {
  		    	e.preventDefault();
  		    	$this.toggleClass('is-open');
  		    	$this.next('.footer__menu-lists').slideToggle();
  		    }
  		})

  		$('.footer__menu-title').each( function (e) {
  		    var $this = $(this);
  		    if ($this.next('.footer__menu-lists').length > 0 ) {
  		    	$this.addClass('chevron');
  		    }
  		})
  	}



  	if ($(window).width() <= 758) {
  		if($('.slider-single').length > 0) {

  			$('.slider-single').slick({
  			    slidesToShow: 1,
  			    infinite: true,
  			    dots: true,
  			    arrows: false,
  			    slidesToScroll: 1,
  			});
  		}
  	} 



  	if ($(window).width() <= 758) {
  		if($('.slider-center').length > 0) {

  			$('.slider-center').slick({
  			    slidesToShow: 1,
  			    infinite: false,
  			    dots: false,
  			    arrows: false,
  			    slidesToScroll: 1,
  			    centerMode: true,
  			    // autoplay: true,
  			    // autoplaySpeed: 2000,
  			    centerPadding: '200px',
  			    responsive: [
  			        {
  			            breakpoint: 670,
  			            settings: {
  			                centerPadding: '150px',
  			            }
  			        },
  			        {
  			            breakpoint: 580,
  			            settings: {
  			                centerPadding: '100px',
  			            }
  			        },
  			        {
  			            breakpoint: 480,
  			            settings: {
  			                centerPadding: '70px',
  			            }
  			        },

  			        {
  			            breakpoint: 440,
  			            settings: {
  			                centerPadding: '55px',
  			            }
  			        },

  			        {
  			            breakpoint: 350,
  			            settings: {
  			                centerPadding: '30px',
  			            }
  			        },
  			    ]
  			});
  		}
  	}

    if ($(window).width() <= 758) {
      if($('.slider-center-2').length > 0) {

        var $status = $('.rate__counter');
        var $slickElement = $('.slider-center-2');

        $slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
            //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
            var i = (currentSlide ? currentSlide : 0) + 1;
            $status.text(i + '/' + slick.slideCount);
        });

        $('.slider-center-2').slick({
            slidesToShow: 1,
            infinite: false,
            dots: false,
            arrows: false,
            slidesToScroll: 1,
            centerMode: true,
            // autoplay: true,
            // autoplaySpeed: 2000,
            centerPadding: '300px',
            responsive: [
                {
                    breakpoint: 670,
                    settings: {
                        centerPadding: '250px',
                    }
                },
                {
                    breakpoint: 580,
                    settings: {
                        centerPadding: '180px',
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        centerPadding: '120px',
                    }
                },

                {
                    breakpoint: 440,
                    settings: {
                        centerPadding: '90px',
                    }
                },

                {
                    breakpoint: 350,
                    settings: {
                        centerPadding: '57px',
                    }
                },
            ]
        });
      }
    }

  	if ($(window).width() <= 758) {
  		if($('.slider-two-img').length > 0) {

  			$('.slider-two-img').slick({
  			    slidesToShow: 2,
  			    infinite: false,
  			    dots: true,
  			    arrows: false,
  			    slidesToScroll: 1,
  			});
  		}
  	} 

  	if ($(window).width() <= 580) {
  		if($('.slider-news').length > 0) {

  			$('.slider-news').slick({
  			    slidesToShow: 1,
  			    infinite: true,
  			    dots: true,
  			    arrows: false,
  			    slidesToScroll: 1,
  			});
  		}
  	} 


    if ($(window).width() <= 580) {
      if($('.slider-gallery').length > 0) {

        $('.slider-gallery').slick({
            slidesToShow: 1,
            infinite: false,
            dots: false,
            arrows: false,
            slidesToScroll: 1,
            centerMode: true,
            // autoplay: true,
            // autoplaySpeed: 2000,
            centerPadding: '120px',
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        centerPadding: '130px',
                    }
                },

                {
                    breakpoint: 440,
                    settings: {
                        centerPadding: '95px',
                    }
                },

                {
                    breakpoint: 350,
                    settings: {
                        centerPadding: '62px',
                    }
                },
            ]
        });
      }
    }



    $('.rate__price-container').on('click', function () {
        $(this).children().siblings().find(".rate__price-month").toggleClass('show');
        $(this).next().toggleClass('active');
    })

    $('.service-single__more-btn').on('click', function () {
        $(this).parent().next().slideToggle();
        $(this).toggleClass('active');
        $(this).parent().find('.service-single__text').first().toggleClass('active');
    })

    $('.contacts__info-btn-more').on('click', function () {
        $('.contacts__info--hider').addClass('active');
        $(this).addClass('hide');
    })


    // var OSName="Unknown OS";
    // if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
    // else if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
    // else if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
    // else if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";

    // console.log('Your OS is: '+OSName);

    // if (navigator.appVersion.indexOf("Win")!=-1) {
    //     $('.btn-main').addClass('wert');
    // }

  
});



$(document).ready(function () {
    function pageWidget(pages) {
        var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
        widgetWrap.prependTo("body");
        for (var i = 0; i < pages.length; i++) {
            $('<li class="widget_item"><a class="widget_link" href="' + pages[i][0] + '.html' + '">' + pages[i][1] + '</a></li>').appendTo('.widget_list');
        }
        var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:absolute;top:0;left:0;z-index:9999;padding:5px 10px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_list{padding: 0;}.widget_item{list-style:none;padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
        widgetStilization.prependTo(".widget_wrap");
    }

    pageWidget([
        ['index', 'Главная'],
        ['servers', 'Серверы'],
        ['servers-single', 'Серверы Отдельная'],
        ['cms-service', 'Хост-услуги'],
        ['cms-service-2', 'Хост-услуги-2'],
        ['services', 'Услуги'],
        ['service-single', 'Услуги-отдельная'],
        ['about', 'О компании'],
        ['data-center', 'Дата-Центр'],
        ['contacts', 'Контакты'],
        ['terms', 'Правила-пользования'],
    ]);
});